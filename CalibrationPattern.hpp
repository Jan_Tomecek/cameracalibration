//
// Created by jan on 8/7/21.
//

#ifndef CALIBRATION_CALIBRATIONPATTERN_HPP
#define CALIBRATION_CALIBRATIONPATTERN_HPP

#include <vector>

class CalibrationPattern{

public:
    std::vector<cv::Point2d> points2d_;
    std::vector<cv::Point3d> points3d_;

    struct Params{
        struct Origin{
            double x{0.};
            double y{0.};
            double z{0.};
        } origin_;
    };

    virtual const std::vector<cv::Point2d>& getPoints2d() const = 0;
    virtual const std::vector<cv::Point3d>& getPoints3d() const = 0;

private:
    Params params_;
};


class ChessboardCalibrationPattern : public CalibrationPattern{

public:
    struct Params : public CalibrationPattern::Params {
        struct SquareSize{
            double w{0.};
            double h{0.};
        } square_size_;
        struct SquareNum{
            uint32_t x{0};
            uint32_t y{0};
        } square_num_;
    };

    ChessboardCalibrationPattern(const Params& params) : params_{params} {
        for(int i = 0; i < params_.square_num_.x*params_.square_num_.y; i++) {
            points2d_.push_back(cv::Point2d((i / params_.square_num_.y) * params_.square_size_.w, (i % params_.square_num_.y) * params_.square_size_.h));
            points3d_.push_back(cv::Point3d((i / params_.square_num_.y) * params_.square_size_.w, (i % params_.square_num_.y) * params_.square_size_.h, 0.));
        }
    }

    auto getPoints2d() const -> const std::vector<cv::Point2d>& {
        return points2d_;
    }

    auto getPoints3d() const -> const std::vector<cv::Point3d>& {
        return points3d_;
    }

private:
    Params params_;
};
#endif //CALIBRATION_CALIBRATIONPATTERN_HPP
