//
// Created by jan on 8/7/21.
//

#ifndef CALIBRATION_CALIBRATIONALGORITHMS_HPP
#define CALIBRATION_CALIBRATIONALGORITHMS_HPP


class CalibrationAlgorithm{

public:
    virtual auto getR() const -> const cv::Mat& = 0;
    virtual auto getT() const -> const cv::Mat& = 0;
    virtual void calibrate(std::vector<cv::Point3d> points_ref, std::vector<cv::Point2d> points_cam) = 0;

};

class CalibrationAlgorithmHomography : public CalibrationAlgorithm{

private:
    cv::Mat R_;
    cv::Mat T_;

public:
    auto getR() const -> const cv::Mat&{
        return R_;
    }

    auto getT() const -> const cv::Mat&{
        return T_;
    }

    void calibrate (std::vector<cv::Point3d> points_ref, std::vector<cv::Point2d> points_cam) {
        std::vector<cv::Point2d> points_ref_2d;
        for(const auto& p3d : points_ref) {
            points_ref_2d.push_back(cv::Point2d(p3d.x, p3d.y));
        }
        cv::Mat H = cv::findHomography(points_ref_2d, points_cam);
        double lambda = 1./ cv::norm(H.col(0));
        cv::Mat T = lambda*H.col(2);
        cv::Mat r_col0 = lambda*H.col(0);
        cv::Mat r_col1 = lambda*H.col(1);
        cv::Mat r_col2 = r_col0.cross(r_col1);

        cv::Mat R;
        cv::hconcat(r_col0, r_col1, R);
        cv::hconcat(R, r_col2, R);

        cv::Mat S,U,Vt;
        SVDecomp(R, S, U, Vt, cv::SVD::FULL_UV);
        R = U*Vt;

        R_ = R;
        T_ = -T.t()*R;
    }
};

class CalibrationAlgorithmPnP : public CalibrationAlgorithm{

private:
    cv::Mat R_;
    cv::Mat T_;

public:
    auto getR() const -> const cv::Mat&{
        return R_;
    }

    auto getT() const -> const cv::Mat&{
        return T_;
    }

    void calibrate (std::vector<cv::Point3d> points_ref, std::vector<cv::Point2d> points_cam) {
        cv::Mat r, t, inliers;
        cv::solvePnPRansac (points_ref, points_cam, cv::Mat::eye(3,3,CV_64FC1), cv::Mat(), r, t, true,100, 1, 0.99,  inliers, cv::SOLVEPNP_ITERATIVE);
        cv::Mat R_cv;
        cv::Rodrigues(r, R_cv);

        R_ = R_cv;
        T_ = -t.t()*R_cv;
    }
};

class PatternToVehicleConversion {
private:
    cv::Mat r;
    cv::Mat T;
public:
    void operator() (CalibrationPattern::Params pattern_params, std::shared_ptr<CalibrationAlgorithm> calib_algo)
    {
        cv::Mat r_temp;
        cv::Mat T_temp;
        cv::Rodrigues(calib_algo->getR(), r_temp);
        T_temp = calib_algo->getT();
        T_temp.copyTo(T);

        T.at<double>(0,0) = -T_temp.at<double>(0,1) + pattern_params.origin_.x;
        T.at<double>(0,1) = -T_temp.at<double>(0,0) + pattern_params.origin_.y;
        T.at<double>(0,2) = -T_temp.at<double>(0,2) + pattern_params.origin_.z;

        r = cv::Mat(1,3,CV_64FC1);
        r.at<double>(0,0) = CV_PI/2. + r_temp.at<double>(0,0);
        r.at<double>(0,1) = - r_temp.at<double>(0,1);
        r.at<double>(0,2) = - r_temp.at<double>(0,2);
    }

    auto getr() const -> const cv::Mat& {
        return r;
    }

    auto getT() const -> const cv::Mat& {
        return T;
    }
};

#endif //CALIBRATION_CALIBRATIONALGORITHMS_HPP
