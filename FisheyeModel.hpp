//
// Created by jan on 8/7/21.
//

#ifndef CALIBRATION_FISHEYEMODEL_HPP
#define CALIBRATION_FISHEYEMODEL_HPP

class FisheyeModel {
public:
    struct Params {
        struct Center{
            double x{0.};
            double y{0.};
        } center_;
        struct Distortion{
            double a0{0.};
            double a1{0.};
            double a2{0.};
            double a3{0.};
        } distortion_;
        double scale{0.};
    };

    FisheyeModel(Params params) : params_{params} {};
    auto img2world(const std::vector<cv::Point2d>& points_img ) -> std::vector<cv::Point2d> {
        std::vector<cv::Point2d> points_world;

        for( const auto& point : points_img ) {
            double x_c = point.x - params_.center_.x;
            double y_c = point.y - params_.center_.y;

            double r = sqrt(x_c*x_c + y_c*y_c);
            double pol = params_.distortion_.a0 + params_.distortion_.a1*r*r + params_.distortion_.a2*r*r*r + params_.distortion_.a3*r*r*r*r;
            double point_norm = sqrt(x_c*x_c + y_c*y_c + pol*pol);

            if(point_norm > 0.){
                double X = x_c/point_norm;
                double Y = y_c/point_norm;
                double Z = pol/point_norm;
                points_world.push_back(cv::Point2d(X/Z, Y/Z));
            }
        }

        return points_world;
    }

private:
    Params params_;
};


#endif //CALIBRATION_FISHEYEMODEL_HPP
